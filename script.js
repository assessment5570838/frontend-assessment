const userSlider = document.getElementById('userSlider');
const userValue = document.getElementById('userValue');
const cards = document.querySelectorAll('.card');
const form = document.getElementById('myForm');




// form.addEventListener('submit', function (event) {
//     event.preventDefault();
//     const endpoint = "forms.maakeetoo.com/formapi/533";
//     const accessCode = "X45AZ037ESZ523PETPKDUY7NR";
//     fetch(endpoint, {
//         method: 'POST',
//         headers: {
//             'CODE': accessCode
//         }
//     })
//         .then((response) => {
//             if (!response.ok) {
//                 // console.error('Network response error:', response.status, response.statusText);
//                 throw new Error('Network response was not ok');
//             }
//             return response.json();
//         })
//         .then((responseText) => {
//             return responseText
//         })
//         .catch((error) => {
//             console.log('Error:', error)
//         })
// })


//to highlight the appropriate plan


form.addEventListener('submit', (e) => {
    e.preventDefault();

    const formData = new FormData(form);

    const res = Object.fromEntries(formData);
    const payload = JSON.stringify(res);
    console.log(payload)

    for (let item of formData) {
        console.log(item[0], item[1])
    };

    fetch('https://httpbin.org/post', {
        method: "POST",
        body: payload,
        headers: {
            'Content-Type': 'application/json'
        }
    })

        .then(res => res.json())
        .then(res => {
            console.log(res);
            form.reset()
        })


})




function highlightPlan(userCount) {
    cards.forEach((card, index) => {
        if (userCount >= index * 10 + 1 && userCount <= (index + 1) * 10) {
            card.classList.add('highlighted')
        } else {
            card.classList.remove('highlighted')
        }
    })
}




//to get the slider input
function sliderValue() {
    const value = parseInt(this.value);
    userValue.innerText = value;
    highlightPlan(value)
}

userSlider.addEventListener('input', sliderValue)


userSlider.value = 0;
userValue.textContent = 0
highlightPlan(0)


// 1.a function to determine which price card to highlight
// 2.catch the changing value of the slider 


